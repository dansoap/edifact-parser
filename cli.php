<?php
use Dansoap\Edifact\Parser\SegmentIterator;
use Dansoap\Edifact\Parser\MessageIterator;
use Dansoap\Edifact\Definition\Loader;
use Dansoap\Edifact\DocumentParser;
use Dansoap\Edifact\MessageParser;

require_once 'vendor/autoload.php';

// $segmentIterator = new SegmentIterator(file_get_contents('data/ORDRSP.edi'));

// foreach ($segmentIterator as $key => $rawSegment) {
// printf("%3d: %s\n", $key, $rawSegment);
// }

// echo "#######################\n";

// $segmentIterator = new SegmentIterator(file_get_contents('data/ORDERS.3.edi'));

// foreach ($segmentIterator as $key => $rawSegment) {
// printf("%3d: %s\n", $key, $rawSegment);
// }

// echo "#######################\n";

$definitionLoader = new Loader(__DIR__ . '/definition');
$parser = new MessageParser($definitionLoader);

$messages = new MessageIterator(file_get_contents('data/ORDERS.multi.edi'));

foreach ($messages as $key => $message) {
    
    echo PHP_EOL . "Message #" . $key . PHP_EOL . "----------------------" . PHP_EOL;
    
    $parser->parse($message);
}