<?php
namespace Dansoap\Edifact\Definition\Message;

class Element
{

    /**
     *
     * @var string
     */
    public $id;
    
    /**
     * 
     * @var string
     */
    public $path;

    /**
     *
     * @var int
     */
    public $maxRepeat;

    /**
     *
     * @var bool
     */
    public $required;
}

