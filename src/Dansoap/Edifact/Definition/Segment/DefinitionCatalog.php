<?php
namespace Dansoap\Edifact\Definition\Segment;

class DefinitionCatalog
{

    protected $version;

    /**
     *
     * @var SegmentDefinition[]
     */
    protected $definitions = [];

    public function __construct($version)
    {
        $this->version = $version;
    }

    /**
     *
     * @param SegmentDefinition $segmentDefinition            
     */
    public function addSegmentDefinition(SegmentDefinition $segmentDefinition)
    {
        $this->definitions[$segmentDefinition->getId()] = $segmentDefinition;
    }

    /**
     *
     * @param string $id            
     * @return SegmentDefinition
     */
    public function getSegmentDefinition($id)
    {
        return $this->definitions[$id];
    }
}

