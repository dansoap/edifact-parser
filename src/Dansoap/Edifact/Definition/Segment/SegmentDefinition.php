<?php
namespace Dansoap\Edifact\Definition\Segment;

class SegmentDefinition
{

    protected $id;

    protected $name;

    protected $description;
    
    protected $dataElements = [];
}

