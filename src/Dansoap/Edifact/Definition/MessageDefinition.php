<?php
namespace Dansoap\Edifact\Definition;

use Dansoap\Edifact\Definition\Message\Group;

class MessageDefinition
{

    protected $version;

    protected $messageType;

    protected $elements = [];

    /**
     *
     * @var Group
     */
    protected $root;

    public function __construct($version, $messageType)
    {
        $this->version = $version;
        $this->messageType = $messageType;
    }

    public function setRootGroup(Group $root)
    {
        $this->root = $root;
    }

    public function getRootGroup()
    {
        return $this->root;
    }

    public function setElements($elements)
    {
        $this->elements = $elements;
    }

    public function getElementByPath($path)
    {
        return $this->elements[$path];
    }
}
