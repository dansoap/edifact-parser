<?php
namespace Dansoap\Edifact\Definition;

use Dansoap\Edifact\Definition\Message\Segment;
use Dansoap\Edifact\Definition\Message\Group;
use Dansoap\Edifact\Definition\MessageDefinition;

class Loader
{

    protected $dataPath;

    protected $elementsByPath = [];

    protected function getFileNameForMessage($version, $messageType)
    {
        return sprintf('%s/EDI_EDIFACT_%s/messages/%s.xml', $this->dataPath, strtoupper($version), strtolower($messageType));
    }

    protected function getFileNameForDefinition($version, $definitionType)
    {
    	return sprintf('%s/EDI_EDIFACT_%s/%s.xml', $this->dataPath, strtoupper($version), strtolower($definitionType));
    }

    public function __construct($dataPath)
    {
        $this->dataPath = $dataPath;
    }

    /**
     *
     * @param string $version            
     * @param string $messageType            
     * @return \Dansoap\Edifact\Definition\MessageDefinition
     */
    public function loadMessageDefinition($version, $messageType)
    {
        $definitionFilename = $this->getFileNameForMessage($version, $messageType);
        if (! file_exists($definitionFilename)) {
            throw new \Exception('Message definition file not found, tried to load ' . $definitionFilename);
        }
        $xml = new \SimpleXMLElement(file_get_contents($definitionFilename));
        
        $rootGroup = new Group();
        $rootGroup->id = '/';
        
        $this->elementsByPath = [];
        
        $this->walkMessageDefinitionGroup($xml, $rootGroup);
        
        $definition = new MessageDefinition(strtoupper($version), strtoupper($messageType));
        $definition->setRootGroup($rootGroup);
        $definition->setElements($this->elementsByPath);
        
        return $definition;
    }

    /**
     *
     * @param \SimpleXMLElement $xml            
     * @param Group $currentGroup            
     */
    protected function walkMessageDefinitionGroup(\SimpleXMLElement $xml, Group $currentGroup)
    {
        foreach ($xml as $element) {
            
            if ($element->getName() != 'segment' && $element->getName() != 'group') {
                continue;
            }
            
            switch ($element->getName()) {
                case 'segment':
                    $data = new Segment();
                    break;
                case 'group':
                    $data = new Group();
                    break;
            }
            
            foreach ($element->attributes() as $key => $value) {
                switch ($key) {
                    case 'id':
                        $data->id = (string) $value;
                        break;
                    case 'maxrepeat':
                        $data->maxRepeat = (int) $value;
                        break;
                    case 'required':
                        $data->required = ($value == 'true');
                        break;
                }
            }
            
            $data->path = trim($currentGroup->path . '/' . $data->id, '/');
            
            if ($data instanceof Group) {
                $this->walkMessageDefinitionGroup($element, $data);
            }
            
            $currentGroup->children[$data->id] = $data;
            $this->elementsByPath[$data->path] = $data;
        }
    }
}

