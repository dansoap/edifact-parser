<?php
namespace Dansoap\Edifact;

class RawMessage
{

    public $header;

    /**
     *
     * @var RawSegment[]
     */
    public $segments;

    public $version;

    public $type;

    public function __construct($header)
    {
        $this->header = $header;
        $this->parseHeader();
    }

    /**
     */
    protected function parseHeader()
    {
        preg_match('/([A-Z]{6}).([A-Z]).([0-9]{2}[A-Z])/', $this->header, $matches);
        $this->version = $matches[2] . $matches[3];
        $this->type = $matches[1];
    }

    /**
     */
    public function __toString()
    {
        $return = [
            $this->header . PHP_EOL
        ];
        foreach ($this->segments as $key => $segment) {
            $return[] = sprintf("%3d: %s%s", $key, $segment, PHP_EOL);
        }
        return join("", $return);
    }
}

