<?php
namespace Dansoap\Edifact;

class Edifact
{

    const DEFINITION_CODES = 'codes';

    const DEFINITION_COMPOSITE_DATA_ELEMENTS = 'composite_data_elements';

    const DEFINITION_DATA_ELEMENTS = 'data_elements';

    const DEFINITION_PACKAGE = 'package';

    const DEFINITION_SEGMENTS = 'segments';

    const DEFINITION_SERVICE_CODES = 'service_codes';
    
}

