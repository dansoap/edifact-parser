<?php
namespace Dansoap\Edifact;

use Fhaculty\Graph\Vertex;

class SyntaxWalker
{

    protected $syntax;

    /**
     *
     * @var Vertex
     */
    protected $current;

    public function __construct(SyntaxGraph $syntax)
    {
        $this->syntax = $syntax;
        $this->current = $syntax->getVertexFirst();
    }

    public function getCurrent()
    {
        return $this->current;
    }

    public function gotoNext($id)
    {
        // TODO: find path to next allowed segment with name "id"
    }
}

