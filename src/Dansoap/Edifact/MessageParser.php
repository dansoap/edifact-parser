<?php
namespace Dansoap\Edifact;

use Dansoap\Edifact\Definition\MessageDefinition;
use Dansoap\Edifact\Definition\Loader;

class MessageParser
{

    /**
     *
     * @var MessageDefinition
     */
    protected $messageDefinition = null;

    /**
     *
     * @var Definition\Loader
     */
    protected $definitionLoader = null;

    /**
     *
     * @param Definition\Loader $definitionLoader            
     */
    public function __construct(Loader $definitionLoader)
    {
        $this->definitionLoader = $definitionLoader;
    }

    public function validate(RawMessage $message) {
        $this->messageDefinition = $this->definitionLoader->loadMessageDefinition($message->version, $message->type);
        $syntax = new SyntaxGraph($this->messageDefinition);
        $syntaxWalker = new SyntaxWalker($syntax);
        
        foreach ($message->segments as $segment) {
            $syntaxWalker->gotoNext($segment->id);
        }
    }
    
    /**
     *
     * @param string $message            
     */
    public function parse(RawMessage $message)
    {
        $this->messageDefinition = $this->definitionLoader->loadMessageDefinition($message->version, $message->type);
        
        $syntax = new SyntaxGraph($this->messageDefinition);
//         $syntax->paint();
//         die();
        
//         $message->segments = 
        
        /** @var RawSegment $rawSegment */
        foreach ($message->segments as $rawSegment) {
            var_dump($rawSegment->id);
            var_dump($rawSegment->data);
        }
    }

}

