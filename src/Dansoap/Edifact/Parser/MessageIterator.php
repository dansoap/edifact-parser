<?php
namespace Dansoap\Edifact\Parser;

use Dansoap\Edifact\RawMessage;

/**
 * Iterates over an EDIFACT string, separates messages from 
 * each other and returns items representing a raw EDIFACT message
 *
 * @author Daniel Seif <dansoap@castex.de>
 *        
 */
class MessageIterator implements \Iterator
{

    /**
     *
     * @var SegmentIterator
     */
    protected $segmentIterator;

    /**
     *
     * @var RawMessage
     */
    protected $currentMessage;

    protected $currentMessageKey;

    protected $endReached;

    /**
     * Creates the Iterator. Input is a string containing 1..n EDIFACT messages
     *
     * @param string $rawdocument            
     */
    public function __construct($rawdocument)
    {
        $this->segmentIterator = new SegmentIterator($rawdocument);
        $this->currentMessage = null;
    }
    
    /*
     * (non-PHPdoc) @see Iterator::current()
     */
    public function current()
    {
        if ($this->currentMessage == null) {
            $this->next();
        }
        return $this->currentMessage;
    }
    
    /*
     * (non-PHPdoc) @see Iterator::next()
     */
    public function next()
    {
        while ($this->segmentIterator->current()->id != 'UNH' && $this->segmentIterator->valid()) {
            $this->segmentIterator->next();
        }
        
        if (! $this->segmentIterator->valid()) {
            $this->endReached = true;
            return;
        }
        
        $this->currentMessage = new RawMessage($this->segmentIterator->current());
        
        do {
            $this->currentMessage->segments[] = $this->segmentIterator->current();
            $this->segmentIterator->next();
        } while ($this->segmentIterator->current()->id != 'UNT');
        $this->currentMessage->segments[] = $this->segmentIterator->current();
        $this->currentMessageKey ++;
    }
    
    /*
     * (non-PHPdoc) @see Iterator::key()
     */
    public function key()
    {
        return $this->currentMessageKey;
    }
    
    /*
     * (non-PHPdoc) @see Iterator::valid()
     */
    public function valid()
    {
        return ! $this->endReached;
    }
    
    /*
     * (non-PHPdoc) @see Iterator::rewind()
     */
    public function rewind()
    {
        $this->segmentIterator->rewind();
        $this->currentMessageKey = - 1;
    }
}

