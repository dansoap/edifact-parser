<?php
namespace Dansoap\Edifact\Parser;

use Dansoap\Edifact\RawSegment;

class SegmentIterator implements \Iterator
{

    protected $componentSeparator = ':';

    protected $dataElementSeparator = '+';

    protected $decimalNotification = '.';

    protected $releaseCharacter = '?';

    protected $segmentTerminator = '\'';

    protected $message;

    /**
     * the index of the last character read
     *
     * @var int
     */
    protected $currentCharacter;

    protected $maxCharacter;

    /**
     *
     * @var RawSegment
     */
    protected $currentSegment = null;

    protected $currentSegmentKey = - 1;

    protected $eom;

    protected $valid;

    public function __construct($message)
    {
        if (substr($message, 0, 3) == 'UNA') {
            $this->initializeSeparators(substr($message, 0, 9));
        }
        $this->message = $message;
        $this->maxCharacter = strlen($message);
        $this->rewind();
    }

    protected function initializeSeparators($unaSegment)
    {
        $unaSegment = substr($unaSegment, 3);
        preg_match('/(.)(.)(.)(.) (.)/', $unaSegment, $matches);
        array_shift($matches);
        list ($this->componentSeparator, $this->dataElementSeparator, $this->decimalNotification, $this->releaseCharacter, $this->segmentTerminator) = $matches;
    }

    protected function findNextSegment()
    {
        if ($this->eom) {
            $this->valid = false;
            return;
        }
        
        $segmentData = '';
        $released = false;
        $endFound = false;
        
        while (! $endFound) {
            $char = $this->message[$this->currentCharacter];
            $segmentData .= $char;
            if ($char == $this->segmentTerminator && ! $released) {
                $endFound = true;
            } elseif ($char == $this->releaseCharacter) {
                $released = true;
            } else {
                $released = false;
            }
            $this->currentCharacter ++;
            if ($this->currentCharacter == $this->maxCharacter) {
                $this->eom = true;
            }
        }
        
        $this->currentSegment = new RawSegment(substr($segmentData, 0, 3), substr($segmentData, 3));
        $this->currentSegmentKey ++;
    }
    
    /*
     * (non-PHPdoc) @see Iterator::current()
     */
    public function current()
    {
        if ($this->currentSegment == null) {
            $this->findNextSegment();
        }
        return $this->currentSegment;
    }
    
    /*
     * (non-PHPdoc) @see Iterator::next()
     */
    public function next()
    {
        $this->findNextSegment();
        return $this->currentSegment;
    }
    
    /*
     * (non-PHPdoc) @see Iterator::key()
     */
    public function key()
    {
        return $this->currentSegmentKey;
    }
    
    /*
     * (non-PHPdoc) @see Iterator::valid()
     */
    public function valid()
    {
        return $this->valid;
    }
    
    /*
     * (non-PHPdoc) @see Iterator::rewind()
     */
    public function rewind()
    {
        $this->currentCharacter = 0;
        $this->currentSegmentKey = - 1;
        $this->eom = false;
        $this->valid = true;
    }
}

