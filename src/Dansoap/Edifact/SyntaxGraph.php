<?php
namespace Dansoap\Edifact;

use Dansoap\Edifact\Definition\MessageDefinition;
use Dansoap\Edifact\Definition\Message\Group;
use Dansoap\Edifact\Definition\Message\Segment;
use Fhaculty\Graph\Graph;
use Fhaculty\Graph\Exporter\Image;

class SyntaxGraph extends Graph
{

    /**
     *
     * @var MessageDefinition
     */
    protected $messageDefinition;

    /**
     * 
     * @var int
     */
    protected $optionalVerticesBoxesCounter = 0;

    /**
     *
     * @param MessageDefinition $messageDefiniton            
     */
    public function __construct(MessageDefinition $messageDefinition)
    {
        parent::__construct();
        $this->messageDefinition = $messageDefinition;
        $this->addGroupToGraph($messageDefinition->getRootGroup());
    }

    /**
     *
     * @param Group $group            
     */
    protected function addGroupToGraph(Group $group)
    {
        $groupEntry = $this->createVertex('SegmentGroup ' . $group->path . ' begin');
        
        $previousVertex = $groupEntry;
        
        foreach ($group->children as $child) {
            
            $path = $child->path;
            
            if ($child instanceof Segment) {
                $vertex = $this->createVertex($path, true);
                $startVertex = $endVertex = $vertex;
            } elseif ($child instanceof Group) {
                list ($subGroupEntry, $subGroupExit) = $this->addGroupToGraph($child);
                $startVertex = $subGroupEntry;
                $endVertex = $subGroupExit;
            }
            
            $previousVertex->createEdgeTo($startVertex);
            
            if ($child->maxRepeat > 1) {
                $edge = $endVertex->createEdgeTo($startVertex);
                $edge->setCapacity($child->maxRepeat);
            }
            
            if ($child->required != true) {
                $optionalVertex = $this->createVertex($this->optionalVerticesBoxesCounter ++);
                $endVertex->createEdgeTo($optionalVertex);
                $previousVertex->createEdgeTo($optionalVertex);
                $endVertex = $optionalVertex;
            }
            
            $previousVertex = $endVertex;
        }
        
        $groupExit = $this->createVertex('SegmentGroup ' . $group->path . ' exit');
        
        $previousVertex->createEdgeTo($groupExit);
        
        return [
            $groupEntry,
            $groupExit
        ];
    }

    /**
     */
    public function paint($outputFilename)
    {
        $this->setExporter(new Image());
        file_put_contents($outputFilename, (string) $this);
    }
}

