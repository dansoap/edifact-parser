<?php
namespace Dansoap\Edifact;

class RawSegment
{

    public $id;

    public $data;
    
    public $path;

    public function __construct($id, $data)
    {
        $this->id = $id;
        $this->data = $data;
    }

    public function __toString()
    {
        return $this->id . $this->data;
    }
}

